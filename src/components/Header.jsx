import React from 'react';
import LogoElisa from './img/logo_elisa.png';
import './css/header.css';
import Login from './Login';
import Register from './Register';
import espiga from './img/espiga.png';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import './css/Popup.css';
import AgregarRestaurantes from './AgregarRestaurantes.jsx';

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';


export default class Example extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggle2 = this.toggle2.bind(this);


        this.state = {
            isOpen: false,

            restaurante: false,

            esconder: "",

            registrado: true,


            modal: false,


            loginRegister: this.props.login,
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.canvia = this.canvia.bind(this);
    }



    canvia() {
        this.setState({ loginRegister: (this.state.loginRegister === "login") ? "register" : "login" })
    }

    toggleModal(mode = "res") {
        if (mode !== "res") {
            this.setState({
                loginRegister: mode,
                modal: !this.state.modal
            })
        } else {
            this.setState({
                modal: !this.state.modal
            });
        }

    }





    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggle2() {

        this.setState({
            restaurante: !this.state.restaurante
        });
    }


    mouseEnter = () => {
        this.setState({ colorNav: 'red' })
    }
    render() {





        let cosa = (this.state.loginRegister !== "register") ?
            <Login canvia={this.canvia} />
            : <Register canvia={this.canvia} />;




        return (
            <div>

                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/"><img id="logoElisa" src={LogoElisa} alt="Logo_Elisa"></img></NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink onClick={() => this.toggleModal("login")} className="prova" >Iniciar Sesión</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink onClick={() => this.toggleModal("register")} className="prova" >Registrarse</NavLink>
                            </NavItem>
                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret className="prova2" style={{ display: this.state.esconder }}>
                                    Options
                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem onClick={() => this.toggle2()} className="prova3">
                                        Añadir Restaurante
                  </DropdownItem>
                                    <DropdownItem>
                                        Option 2
                  </DropdownItem>
                                    <DropdownItem divider />
                                    <DropdownItem>
                                        Reset
                  </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>

                <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className}>
                    <div className='box'><img alt='' className='logo' src={espiga} /></div>
                    <ModalBody>
                        <div>{cosa}</div>
                    </ModalBody>
                </Modal>


                <Modal isOpen={this.state.restaurante} toggle={this.toggle2} className={this.props.className}>
                    <div className='box'><img alt='' className='logo' src={espiga} /></div>
                    <ModalBody>
                        <div><AgregarRestaurantes /></div>
                    </ModalBody>
                </Modal>



            </div >
        );
    }
}