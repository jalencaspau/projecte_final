
import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Map extends Component {
  static defaultProps = {
    center: {
      lat: 41.38,
      lng: 2.17
    },
    zoom: 11
  };

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '80vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyCaxyRvCOJu6lwhqw8QEFgODeyIwiMiX54' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={41.38}
            lng={2.17}
            text={<i style={{
              fontSize: '30px',
              color: '#1BB8FF'
            }} className="marker fas fa-map-marker-alt"></i>}
          />
        </GoogleMapReact>

      </div>
    );

  }
}

export default Map;