import React, { Component } from 'react';
import { Redirect} from 'react-router-dom';
import Image from './Image';


export default class Register extends Component {
	constructor() {
		super();

		this.state = {
			nombre: '',
            password: '',
            email:'',
			tornar: false
		};
		// this.submit = this.submit.bind(this);
        this.canvia = this.canvia.bind(this);
    }


	canvia(event) {
		const v = event.target.value;
		const n = event.target.name;
		this.setState({
			[n]: v
		});
	}

	// submit(e) {
	// 	e.preventDefault();
	// 	let nombre = this.state.nombre;
	// 	let password = this.state.password;
	// 	let data = { nombre, password };

	// 	fetch(API + '/usuarios/registre', {
	// 		method: 'POST',
	// 		headers: new Headers({ 'Content-Type': 'application/json' }),
	// 		body: JSON.stringify(data)
	// 	})
	// 		.then((res) => res.json())
	// 		.then((res) => {
	// 			console.log(res);

	// 			if (res.ok === true) {
	// 				return fetch(API + '/usuarios/login', {
	// 					method: 'POST',
	// 					headers: new Headers({ 'Content-Type': 'application/json' }),
	// 					body: JSON.stringify(data)
	// 				});
	// 			} else {
	// 				throw new Error('usuario no registrado');
	// 			}
	// 		})
	// 		.then((res) => res.json())
	// 		.then((res) => {
	// 			const token = res.data;
	// 			console.log(token);
	// 			if (token) {
	// 				console.log('establint cookies');
	// 				this.props.cookies.set('nombre', token.nombre_usuario, { path: '/' });
	// 				this.props.cookies.set('id', token.usuarios_id, { path: '/' });
	// 				this.props.cookies.set('token', token.token, { path: '/' });
	// 				this.setState({ tornar: true });
	// 			}
	// 		})
	// 		.catch((err) => console.log(err));
	// }

	render() {
		if (this.state.tornar === true) {
			return <Redirect to="/" />;
		}

		return (
			<React.Fragment>
				<div className="container h-100">
					<div className="d-flex justify-content-center h-100">
						<div className="user_card">
							<div className="d-flex justify-content-center">
								<div className="brand_logo_container">
                                    
								</div>
							</div>
							<div className="d-flex justify-content-center form_container">
								<form onSubmit={this.submit}>
									<div className="input-group mb-3">
										<div className="input-group-append">
											<span className="input-group-text">
												<i className="fas fa-user" />
											</span>
										</div>
										<input
											type="text"
											onChange={this.canvia}
											name="nombre"
											className="form-control input_user"
											value={this.state.nombre}
											placeholder="nombre"
										/>
									</div>
									
                                    <div className="input-group mb-3">
										<div className="input-group-append">
											<span className="input-group-text">
												<i className="fas fa-key" />
											</span>
										</div>
										<input
											type="password"
											onChange={this.canvia}
											name="password"
											className="form-control input_pass"
											value={this.state.password}
											placeholder="contraseña"
										/>
									</div>
                                    <div className="input-group mb-3">
										<div className="input-group-append">
											<span className="icon input-group-text">
                                                <i className="fas fa-at"></i>
											</span>
										</div>
										<input
											type="email"
											onChange={this.canvia}
											name="email"
											className="form-control input_pass"
											value={this.state.email}
											placeholder="email"
										/>
									</div>
                                    <Image />
								</form>
							</div>
							<div className="d-flex justify-content-center mt-3 login_container">
								<button onClick={this.submit} type="button" name="button" className="btn login_btn">
									Registrate
								</button>
							</div>
							<div className="mt-4">
								<div className="login">
                                    Ya tienes una cuenta?
                                </div>
								<p className="change" onClick={this.props.canvia}>  Inicia Sesión</p>
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
		);
	}
}
