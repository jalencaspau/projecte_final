import React from 'react';
import AvatarImageCropper from 'react-avatar-image-cropper';



export default class Image extends React.Component {

  apply(file) {
  	console.log(file)
  }
  render() {
    return (
      <div className="divGeneral" style={{ width:'250px',height:'250px',margin:'auto'}}>
        <AvatarImageCropper applyBtnStyle={{backgroundColor: '#FFCA49' }} text= "Carga tu foto" apply={this.apply.bind(this)} isBack={true}/>
      </div>
    );
  }
}
