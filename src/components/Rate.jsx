import React from 'react';

import star from './img/star.png';
import starYellow from './img/star_yellow.png';

import './css/Rate.css';

const SERVER = 'http://localhost:3001/API/phrases';


export default class Rate extends React.Component {
    constructor(props) {
        super(props)
        let puntuacionTotal = 3;
        //this.props.data.puntuacionTotal * 1;
        let puntuado = 3;
        //this.props.data.puntuado * 1;
        this.state = {
            rating: (puntuacionTotal / puntuado),
            stars: [],


        }
        console.log("puntuacion: " + (this.state.rating));
    }

    setRating(rating) {
        if (this.state.disabled) return;
        this.setState({ rating });
        console.log("rating " + rating);

        let rate = {
            puntuacion: rating
        };


        //     rate = JSON.stringify(rate);
        //     let fetchUrl = SERVER + "/rate/" + this.props.data.id;
        //     console.log("fetch to: " + fetchUrl);
        //     fetch(fetchUrl, {
        //         method: 'PUT',
        //         headers: new Headers({ 'Content-Type': 'application/json' }),
        //         body: rate
        //     })
        //         .then(resp => resp.json())
        //         .catch(err => console.log("Error Rating: " + err));

        this.setState({ volver: true });

    }

    displayStars() {
        let stars = [];
        let disabled = this.state.disabled ? 'disabled' : '';
        for (let i = 1; i <= 5; i++) {
            stars.push(<img className={(disabled + " stars")} alt="rating" key={i}
                src={i <= this.state.rating ? starYellow : star}
            ></img>)
        };

        this.setState({
            stars: stars
        })

    };

    render() {
        let stars = [];

        if (this.props.ratingActive === "false") {

            for (let i = 1; i <= 5; i++) {
                stars.push(<img className="inactive stars" alt="rating" key={i}
                    src={i <= this.state.rating ? starYellow : star} 
                ></img>)
            }
        } else {
            let disabled = this.state.disabled ? 'disabled' : '';
            for (let i = 1; i <= 5; i++) {
                stars.push(<img className={(disabled + "active stars")} alt="rating" key={i}
                    src={i <= this.state.rating ? starYellow : star} onClick={() => {
                        this.setRating(i)
                        this.setState({ disabled: true })
                    }}></img>)
            }


        }

        return (
            <div className='stars-container'>
                {stars}
            </div>
        );
    }

}
