
import React from 'react';
import { Button, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';
import restauranteprueba from './img/restauranteprueba.jpeg';
import './css/RestaurantDetail.css';
import './css/Rate.css';
import Rate from './Rate';



export default class RestaurantDetail extends React.Component {
  constructor(props) {
    super(props);
    
    this.toggle = this.toggle.bind(this);
    this.state = {
      popoverOpen: false,
      restaurantClicked: false,
      place: {
        name: "Yatai",
        ranking: 5,
        ranking_count: 4,
        url_foto: {restauranteprueba},
        creator_id: 1,
        latitude: 4,
        longitude: 5,
        created_at: 12
        
      }
  
    };
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }
  

  render() {
    
    if( this.state.restaurantClicked === true) {}
   
    return (
      <div>
         <Button id="PopoverLegacy" type="button">
          Launch Popover (Legacy)
        </Button>
        <UncontrolledPopover trigger="legacy" placement="bottom" target="PopoverLegacy">
          <PopoverHeader>Nombre Restaurante</PopoverHeader>
          <PopoverBody>
              <img className="restaurantImage" src={restauranteprueba} alt="" />
              <Rate ratingActive="false" />
              <p>Ver más >></p>

          </PopoverBody>
        </UncontrolledPopover>
      </div>
    );
  }
}