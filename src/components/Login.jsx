import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';




class Login extends Component {
	constructor() {
		super();
		//Set default message
		this.state = {
			email: '',
			password: '',
			tornar: false
		};
		this.submit = this.submit.bind(this);
        this.canvia = this.canvia.bind(this);
	}

	canvia(event) {
		const v = event.target.value;
		const n = event.target.name;
		this.setState({
			[n]: v
		});
    }
    
  
	submit(e) {
		e.preventDefault();
		let email = this.state.email;
		let password = this.state.password;
        let data = { email, password };
        
    }


	render() {
		if (this.state.tornar === true) {
			return <Redirect to="/index" />;
		}

		return (
				<div className="login container h-100">
					<div className="d-flex justify-content-center h-100">
						<div className="user_card">
							<div className="d-flex justify-content-center">
								<div className="brand_logo_container">
									
								</div>
							</div>
							<div className="d-flex justify-content-center form_container">
								<form onSubmit={this.submit}>
									<div className="input-group mb-3">
										<div className="input-group-append">
											<span className="input-group-text">
												<i className="fas fa-at"></i>
											</span>
										</div>
										<input
											type="text"
											onChange={this.canvia}
											name="email"
											className="form-control input_user"
											value={this.state.email}
											placeholder="email"
										/>
									</div>
									<div className="input-group mb-2">
										<div className="input-group-append">
											<span className="input-group-text">
												<i className="fas fa-key" />
											</span>
										</div>
										<input
											type="password"
											onChange={this.canvia}
											name="password"
											className="form-control input_pass"
											value={this.state.password}
											placeholder="contraseña"
										/>
									</div>
								</form>
							</div>
							<div className="d-flex justify-content-center mt-3 login_container">
								<button onClick={this.submit} type="button" name="button" className="btn login_btn">
									Login
								</button>
                            </div>
							<div className="mt-4">
								<div className="login">
                                No tienes una cuenta?
                                </div>
								<p className="change" onClick={this.props.canvia}>  Registrate!</p>
                            </div>
						</div>
					</div>
				</div>
		
		);
	}
}

export default Login;

