import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Rate from './Rate.jsx';
import Image from './Image.jsx';
import Autocomplete from './Autocomplete.jsx';




export default class AgregarRestaurantes extends Component {
	constructor() {
		super();
		//Set default message
		this.state = {
			nombre: '',
			rate: '',
			foto: '',
			tornar: false
		};
		this.submit = this.submit.bind(this);
		this.canvia = this.canvia.bind(this);
	}

	canvia(event) {
		const v = event.target.value;
		const n = event.target.name;
		this.setState({
			[n]: v
		});
	}


	submit(e) {
		e.preventDefault();
		let nombre = this.state.nombre;
		let rate = this.state.rate;
		let foto = this.state.foto;
		let data = { nombre, rate, foto };

	}


	render() {
		if (this.state.tornar === true) {
			return <Redirect to="/index" />;
		}

		return (
			<div className="login container h-100">
				<div className="d-flex justify-content-center h-100">
					<div className="user_card">
						<div className="d-flex justify-content-center">
							<div className="brand_logo_container">

							</div>
						</div>
						<div className="d-flex justify-content-center form_container">
							<form onSubmit={this.submit}>
								<div className="input-group mb-3">
									<div className="input-group-append">
										<span className="input-group-text">
											<i className="fas fa-store-alt"></i>
										</span>
									</div>
									<input
										type="text"
										onChange={this.canvia}
										name="nombre"
										className="form-control input_user"
										value={this.state.nombre}
										placeholder="Nombre del restaurante"
									/>
								</div>
								<div>
									<Rate />
								</div>
								<div>
									<Image />
								</div>
								<div className="input-group mb-3">
									<div className="input-group-append">
										<span className="input-group-text">
											<i className="fas fa-map-marker-alt"></i>
										</span>
									</div>
									<Autocomplete />
								</div>
							</form>
						</div>
						<div className="d-flex justify-content-center mt-3 login_container">
							<button onClick={this.submit} type="button" name="button" className="btn login_btn">
								Agrega
								</button>
						</div>
					</div>
				</div>
			</div>

		);
	}
}